from django.contrib.auth.models import User
from django.db import models
import datetime


# Create your models here.
class Book(models.Model):
    """
    Модель книги.
    """
    TITLE_MAX_LENGTH = 128
    AUTHOR_MAX_LENGTH = 128
    ISBN_MAX_LENGTH = 18

    isbn = models.CharField(max_length=ISBN_MAX_LENGTH,
                            unique=True)
    title = models.CharField(max_length=TITLE_MAX_LENGTH)
    author = models.CharField(max_length=AUTHOR_MAX_LENGTH)
    year = models.IntegerField(default=2000)
    price_usd = models.IntegerField(default=1)
    price_cent = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Cart(models.Model):
    """
    Модель корзины, у каждого пользователя может быть только 1 корзина.
    """
    books = models.ManyToManyField(Book)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    price_usd = models.IntegerField(default=0)
    price_cent = models.IntegerField(default=0)
    items_count = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        self.update_pricing()
        super(Cart, self).save(*args, **kwargs)

    def update_pricing(self):
        """
        Обновляет информацию о товарах в корзине.
        Актуально в случае изменения цен на книги или
        Добавления новой в корзину.
        """
        self.items_count = self.books.count()
        if self.items_count == 0:
            return 0, 0
        self.price_usd = 0
        self.price_cent = 0
        for book in self.books.all():
            self.price_usd += book.price_usd
            self.price_cent += book.price_cent
        self.price_usd += self.price_cent // 100
        self.price_cent %= 100

    def clear(self):
        """
        Обнуляет всю информацию о товарах в корзине.
        """
        for book in self.books.all():
            self.books.remove(book)
        self.price_usd = 0
        self.price_cent = 0
        self.save()

    def __str__(self):
        return self.user.username + ' cart'


class Order(models.Model):
    """
    Модель заказа, после его оформления,
    из панели администратора можно изменить только его статус.
    """
    STATUS_MAX_LENGTH = 9
    SUBMITTED = 'submitted'
    READY = 'ready'
    DELIVERED = 'delivered'
    ORDER_STATUS_CHOICES = [
        (SUBMITTED, 'submitted'),
        (READY, 'ready'),
        (DELIVERED, 'delivered'),
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    books = models.ManyToManyField(Book, editable=False)
    price_usd = models.IntegerField(default=0, editable=False)
    price_cent = models.IntegerField(default=0, editable=False)
    date = models.DateField(default=datetime.date.today(),
                            editable=False)
    items_count = models.IntegerField(default=0, editable=False)
    status = models.CharField(choices=ORDER_STATUS_CHOICES,
                              max_length=STATUS_MAX_LENGTH,
                              default=SUBMITTED)

    def make(self, cart_id):
        """
        Составляет заказ на основе корзины,
        Чистит эту корзину.
        Принимает параметром Id, т.к. Cart является immutable
        и в противном случае, изменения не сохранятся.

        :param cart_id: Id корзины, из которой мы составляем заказ.
        """
        cart = Cart.objects.get(id=cart_id)
        if cart.items_count == 0:
            return
        self.price_usd = cart.price_usd
        self.price_cent = cart.price_cent
        self.items_count = cart.items_count
        for book in cart.books.all():
            self.books.add(book)
        cart.clear()    # Обнуляет необходимые поля корзины

    def __str__(self):
        return 'Order №%i' % self.id
