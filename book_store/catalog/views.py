from .models import Book, Cart, Order
from .serializers import BookSerializer, CartSerializer, OrderSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.decorators import api_view


def combine(x, y):
    """
    Операция дизъюнкции над множеством и Django QuerySet.
    :param x: set
    :param y: Django QuerySet
    :return: set
    """
    return set(x | set(y))


def filter_numbers(instance, query, is_book=False):
    """
    Проверяет наличие соответствия параметру поиска,
    полям price_usd и price_cent сущности instance.
    :param instance: Django QuerySet с объектами типа Book или Order.
    :param query: параметр поиска.
    :param is_book: Флаг соответствия instance QuerySet с типом Book.
    :return: Успех: QuerySet, удволетворяющий запросу.
             Иначе возвращает пустой массив.
    """
    if query.isdigit():
        if is_book:
            return instance.filter(year=int(query))
    else:
        price_usd, _, price_cent = query.partition('.')
        if price_usd.isdigit() and price_cent.isdigit():
            return instance.filter(price_usd=int(price_usd)) \
                .filter(price_cent=int(price_cent))
    return []


@api_view(['GET'])
def book_search_view(request, query=None):
    """
    Возвращает список книг, удволетворяющий параметру поиска.

    Поиск проходит аналогично панели admin Django:
    Проверяется вхождение параметра в строковые поля и поле года выпуска,
    если тип параметра - вещественный, проверяется
    на соответствие полям price_usd и price_cent.

    :param request:
    :param query: Данные поиска.
    :return: json результат, статус ответа
    """
    books = Book.objects.all()
    search_res = set()

    query = query.lower()
    search_res = combine(search_res,
                         [b for b in books
                          if query in b.title.lower()])
    search_res = combine(search_res,
                         [b for b in books
                          if query in b.author.lower()])
    search_res = combine(search_res,
                         [b for b in books
                          if query in b.isbn.lower()])
    search_res = combine(search_res, filter_numbers(books,
                                                    query,
                                                    is_book=True))

    serializer = BookSerializer(search_res, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def order_search_view(request, query=None):
    """
    Возвращает список заказов, удволетворяющий параметру поиска.

    Поиск проходит аналогично панели admin Django:
    Проверяется вхождение параметра в строковые поля и поле даты,
    если тип параметра - вещественный, проверяется
    на соответствие полям price_usd и price_cent.

    :param request:
    :param query: Данные поиска.
    :return: json результат, статус ответа
    """
    orders = Order.objects.all()
    search_res = set()

    query = query.lower()
    search_res = combine(search_res,
                         [o for o in orders
                          if query in o.user.username.lower()])
    search_res = combine(search_res,
                         [o for o in orders
                          if query in o.status])
    search_res = combine(search_res,
                         filter_numbers(orders, query))
    try:
        search_res = combine(search_res, [o for o in orders
                                          if query in str(o.date)])
    except ValueError:
        pass

    serializer = OrderSerializer(search_res, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


class BookListView(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request):
        """
        Возвращает список книг.
        Вместо price_usd и price_cent указывается price.
        :param request: стандартный request
        :return: json результат, статус ответа
        """
        books = Book.objects.all()

        serializer = BookSerializer(books, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        """
        Добавление книги.
        :param request: стандартный request
        :return: json результат, статус ответа
        """
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)


class OrderListView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        """
        Возвращает список книг.
        :param request: стандартный request
        :return: json результат, статус ответа
        """
        orders = Order.objects.all()

        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)


class CartView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, request):
        """
        Получает объект корзины текущего пользователя из бд,
        если совпадений нет, создает новую корзину
        :param request: стандартный request
        :return: объект Cart
        """
        try:
            return Cart.objects.get(user=request.user)
        except Cart.DoesNotExist:
            return Cart.objects.create()

    def get(self, request):
        """
        Выводит сведения о корзине текущего пользователя
        :param request: стандартный request
        :return: json результат, статус ответа
        """
        cart = self.get_object(request)
        serializer = CartSerializer(cart)
        return Response(serializer.data,
                        status=status.HTTP_200_OK)

    def put(self, request):
        """
        Добавляет предмет в корзину.
        :param request: стандартный request
        :return: json результат, статус ответа
        """
        cart = self.get_object(request)

        try:
            book = Book.objects.get(isbn=request.data['isbn'])
        except Book.DoesNotExist:
            return Response({'error': 'Invalid isbn!'},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            cart.books.get(isbn=book.isbn)
            return Response({'error': 'This book is already in cart!'},
                            status=status.HTTP_409_CONFLICT)
        except Book.DoesNotExist:
            cart.books.add(book)
            serializer = CartSerializer(cart)
            cart.save()
            return Response(serializer.data,
                            status=status.HTTP_200_OK)

    def post(self, request):
        """
        Создает заказ на основе корзины.
        :param request: стандартный request
        :return: json результат, статус ответа
        """
        cart = self.get_object(request)
        order = Order(user=request.user)
        order.save()
        order.make(cart.id)
        if order.items_count == 0:
            return Response({'error': 'Cart is empty!'},
                            status=status.HTTP_204_NO_CONTENT)
        order.save()
        serializer = OrderSerializer(order)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
