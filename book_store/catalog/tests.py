from django.test import TestCase
from catalog.models import Book, Cart, Order
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from datetime import datetime


# Create your tests here.
class OrderTests(TestCase):
    def setUp(self):
        Book.objects.create(title='Зеленая Миля',
                            isbn='978-5-17-086726-4',
                            author='Стивен Кинг', year=2016,
                            price_usd=3, price_cent=99)
        Book.objects.create(title='Три товарища',
                            isbn='978-5-17-066354-5',
                            author='Эрих Мария Ремарк', year=2016,
                            price_usd=5, price_cent=99)
        user = User.objects.create(username='somebody')
        Cart.objects.create(user=user, id=1)

    def test_cart_price_changing(self):
        user = User.objects.get(username='somebody')
        cart = Cart.objects.get(user=user)
        books = Book.objects.all()

        books_price_usd, books_price_cent = 0, 0
        for book in books:
            cart.books.add(book)
            books_price_usd += book.price_usd
            books_price_cent += book.price_cent
        books_price_usd += books_price_cent // 100
        books_price_cent %= 100

        self.assertIs(cart.price_usd == books_price_usd, False)
        cart.save()
        self.assertIs(cart.price_usd == books_price_usd, True)
        self.assertIs(cart.price_cent == books_price_cent, True)

    def test_ordering(self):
        books = Book.objects.all()
        user = User.objects.get(username='somebody')
        cart = Cart.objects.get(user=user)
        for book in books:
            cart.books.add(book)
        cart.save()
        order = Order(user=user,
                      price_usd=cart.price_usd,
                      price_cent=cart.price_cent,
                      items_count=cart.items_count)
        order.save()
        self.assertIs(order.price_usd == cart.price_usd, True)
        self.assertIs(order.price_cent == cart.price_cent, True)
        self.assertIs(order.date == datetime.now().date(), True)
        self.assertIs(order.user.username == cart.user.username, True)

    def test_updating_prices(self):
        user = User.objects.get(username='somebody')
        cart = Cart.objects.get(user=user)
        book = Book.objects.get(id=1)
        cart.books.add(book)
        cart.save()

        old_price = cart.price_usd
        book.price_usd += 1
        book.save()
        cart.save()

        self.assertIs(cart.price_usd - old_price == 1, True)

    def test_book_duplicate_isbn_adding(self):
        error_flag = False
        try:
            book = Book.objects.create(title='Зеленая Миля',
                                       isbn='978-5-17-086726-4',
                                       author='Стивен Кинг', year=2016,
                                       price_usd=3, price_cent=99)
        except IntegrityError:
            error_flag = True
        self.assertIs(error_flag, True)

    def test_book_duplicate_title_adding(self):
        error_flag = False
        try:
            book = Book.objects.create(title='Зеленая Миля',
                                       isbn='978-5-17-086727-4',
                                       author='Стивен Кинг', year=2016,
                                       price_usd=3, price_cent=99)
        except IntegrityError:
            error_flag = True
        self.assertIs(error_flag, False)

