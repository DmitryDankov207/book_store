from __future__ import absolute_import, unicode_literals
from celery import task
from .models import Cart


@task
def update_prices():
    """
    Вызывает метод save для каждой корзины,
    обовляющий стоимость ее товаров.
    Для заказов не вызывается, так как
    после сделки цену менять нельзя.
    """
    for cart in Cart.objects.all():
        cart.update_pricing()
        cart.save()
