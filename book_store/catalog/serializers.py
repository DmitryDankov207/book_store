from rest_framework import serializers
from .models import Book, Cart, Order


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'author', 'price_usd',
                  'price_cent', 'year', 'isbn', )


class CartSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.CharField(
        default=serializers.CurrentUserDefault()
    )
    books = BookSerializer(many=True, read_only=False)

    class Meta:
        model = Cart
        fields = ('id', 'user', 'items_count',
                  'books', 'price_usd', 'price_cent', )


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.CharField(
        default=serializers.CurrentUserDefault()
    )
    books = BookSerializer(many=True, read_only=False)

    class Meta:
        model = Order
        fields = ('id', 'user', 'date', 'price_usd',
                  'price_cent', 'items_count', 'books', 'status', )