from django.contrib import admin
from .models import Book, Cart, Order


# Здесь я реализовал наиболее логичное, на мой взгляд,
# администрирование в панели Django Admin.
class BookAdmin(admin.ModelAdmin):
    list_display = ['title', 'isbn', 'author',
                    'price_usd', 'price_cent', ]
    search_fields = ['title', 'isbn', 'author',]
    list_filter = ['author', 'title', ]


class CartAdmin(admin.ModelAdmin):
    search_fields = ['user', ]
    readonly_fields = ['user', 'price_usd', 'price_cent',
                       'items_count', ]


class OrderAdmin(admin.ModelAdmin):
    search_fields = ['id', 'status']
    list_display = ['id', 'date', 'status',
                    'price_usd', 'price_cent', ]
    list_filter = ['status', 'date', ]
    list_editable = ['status', ]
    readonly_fields = ['user', 'books', 'price_usd',
                       'price_cent', 'date', 'items_count', ]


# Register your models here.
admin.site.register(Book, BookAdmin)
admin.site.register(Cart, CartAdmin)
admin.site.register(Order, OrderAdmin)
