from django.urls import path
from django.contrib import admin
from . import views


# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('cart/', views.CartView.as_view(), name='cart'),
    path('books/', views.BookListView.as_view(), name='book-list'),
    path('books/search/<str:query>',
         views.book_search_view, name='book-search'),
    path('orders/search/<str:query>',
         views.order_search_view, name='order-search'),
    path('orders/', views.OrderListView.as_view(), name='order-list'),
]

