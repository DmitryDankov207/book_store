Book Store REST API
=====================
- admin username: admin
- admin password: qwerty123
- Команды linux выполнял из директроии book_store(корневой для Django проекта)
- Redis запускал командой redis-server
- Celery запускал командой celery -A book_store/book_store worker -l info -B
- Unit тесты запускал командой python manage.py test catalog

Checklist
---
- Запуски тестов API в папке postman_tests.
- Документацию API можно посмотреть в браузере при запуске.
- Unit тесты в файле book_store/catalog/tests.py
- Реализован интерфейс Djando admin

Extra
---
- Celery использует для бэкэнда Redis
- Задание на обновление цен находится в файле book_store/catalog/tasks.py
